package main

import (
	"context"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/go-ble/ble"
	"github.com/go-ble/ble/examples/lib/dev"
	"gopkg.in/alecthomas/kingpin.v2"
)

var RBL_SERVICE_UUID = ble.MustParse("6e400001-b5a3-f393-e0a9-e50e24dcca9e")
var RBL_CHAR_RX_UUID = ble.MustParse("6e400002-b5a3-f393-e0a9-e50e24dcca9e")
var RBL_CHAR_TX_UUID = ble.MustParse("6e400003-b5a3-f393-e0a9-e50e24dcca9e")

var BATT_LEVEL_UUID = ble.MustParse("2A1B")
var BATT_SERVICE_UUID = ble.MustParse("180F")

var done = make(chan struct{})

var (
	debug = kingpin.Flag("debug", "Debug mode.").Short('d').Bool()
	color = kingpin.Flag("color", "3 byte hex color code.").Short('c').String()
	name  = kingpin.Flag("name", "name of device to look for.").Short('n').String()
	fade  = kingpin.Flag("fade", "fade in ms.").Short('f').Int()
)

func main() {
	/*
		To prevent runtime error "cgo argument has Go pointer to Go pointer",
		this env needs to be set.  Since the program is run as a one-off, I'm embedding
		this as the garbage collection issue it represents should be a non-issue.
		This code from https://gist.github.com/CyrusRoshan/6a5283492e9f60dd4a655c0eba54760f
		Gatt error discussed here: https://github.com/paypal/gatt/issues/76
	*/
	if os.Getenv("GODEBUG") != "cgocheck=0" {
		cmd := exec.Command(os.Args[0], os.Args[1:]...)
		env := os.Environ()
		env = append(env, "GODEBUG=cgocheck=0")
		cmd.Env = env
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		err := cmd.Run()
		if err != nil {
			fmt.Println(err)
		}
		os.Exit(0)
	}
	kingpin.Parse()

	d, err := dev.NewDevice("default")
	if err != nil {
		log.Fatalf("Failed to create device, err: %s\n", err)
		return
	}
	ble.SetDefaultDevice(d)

	filter := func(a ble.Advertisement) bool {
		return strings.ToUpper(a.LocalName()) == strings.ToUpper(*name)
	}

	if *debug {
		fmt.Printf("Scanning ...\n")
	}
	p, err := ble.Connect(context.Background(), filter)

	go func() {
		<-p.Disconnected()
		onPeriphDisconnected(p)
	}()

	// Discovery services
	services, err := p.DiscoverServices([]ble.UUID{RBL_SERVICE_UUID, BATT_SERVICE_UUID})
	if err != nil {
		if *debug {
			fmt.Printf("Failed to discover services, err: %s\n", err)
		}
		return
	}

	for _, service := range services {
		switch service.UUID.String() {
		case BATT_SERVICE_UUID.String():
			batteryServiceHandler(p, service)
		case RBL_SERVICE_UUID.String():
			uartServiceHandler(p, service)
		}
	}

	fmt.Printf("Disconnecting [ %s ]... (this might take up to few seconds on OS X)\n", p.Addr())
	p.CancelConnection()

	<-done
	if *debug {
		fmt.Println("Done")
	}
}

func uartServiceHandler(p ble.Client, service *ble.Service) {
	if *debug {
		fmt.Println("Found uart service")
	}

	characteristics, err := p.DiscoverCharacteristics([]ble.UUID{RBL_CHAR_RX_UUID}, service)
	if err != nil {
		if *debug {
			fmt.Printf("Failed to discover characteristics, err: %s\n", err)
		}
		return
	}

	for _, characteristic := range characteristics {
		if characteristic.UUID.String() == RBL_CHAR_RX_UUID.String() {
			if *debug {
				fmt.Println("Found write characteristic")
			}

			var th byte
			var tl byte

			var command = []byte{}
			if *fade != 0 {
				th = byte((*fade / 10) >> 8)
				tl = byte((*fade / 10) & 0xff)
			}
			if *color != "" {
				rgb, err := hex.DecodeString(*color)
				if err != nil || len(rgb) != 3 {
					if *debug {
						fmt.Println("Invalid color code format")
					}
					return
				}

				var led byte
				command = []byte{1, 'c', rgb[0], rgb[1], rgb[2], th, tl, led}

				if *debug {
					fmt.Println("Sending command:", command)
				}
				err = p.WriteCharacteristic(characteristic, command, true)
				if err != nil {
					if *debug {
						fmt.Printf("Failed to send command, err: %s\n", err)
					}
				}
			}
		}
	}
}

func batteryServiceHandler(p ble.Client, service *ble.Service) {
	if *debug {
		fmt.Println("Found battery service")
	}

	characteristics, err := p.DiscoverCharacteristics([]ble.UUID{BATT_LEVEL_UUID}, service)
	if err != nil {
		if *debug {
			fmt.Printf("Failed to discover characteristics, err: %s\n", err)
		}
		return
	}

	for _, characteristic := range characteristics {
		if characteristic.UUID.String() == BATT_LEVEL_UUID.String() {
			if *debug {
				fmt.Println("Found battery characteristic")
			}

			err := p.Subscribe(characteristic, false, func(b []byte) {
				fmt.Printf("Battery: %v%%\n", b[0])
				err := p.Unsubscribe(characteristic, false)
				if err != nil {
					log.Fatalf("unsubscribe failed: %s", err)
				}
				fmt.Printf("unsub\n")

				p.CancelConnection()
			})

			if err != nil {
				log.Fatalf("subscribe failed: %s", err)
			}

			// White the documentation indicates this is synchronous, in practice the result is coming back as a notify
			_, err = p.ReadCharacteristic(characteristic)
			if err != nil {
				log.Fatalf("Failed to read characteristic, err: %s\n", err)
				continue
			}
		}
	}
}

func onPeriphDisconnected(p ble.Client) {
	if *debug {
		fmt.Printf("[ %s ] is disconnected \n", p.Addr())
	}
	close(done)
}
